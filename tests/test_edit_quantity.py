import pytest
from playwright.sync_api import sync_playwright

from pages.cart_page import CartPage
from pages.landing_page import LandingPage
from pages.results_page import ResultsPage


@pytest.fixture(scope="session")
def browser():
    playwright = sync_playwright().start()
    browser = playwright.chromium.launch(headless=True)
    yield browser
    browser.close()
    playwright.stop()


@pytest.fixture(scope="function")
def page(browser):
    page = browser.new_page()
    yield page
    page.close()


def test_edit_quantity(page):
    item = "mouse"
    newQuantity = "3"

    landingPage = LandingPage(page)
    landingPage.navigateToLandingPage()
    landingPage.searchForItem(item)
    print("LANDING PAGE ENDED")
    resultsPage = ResultsPage(page)
    resultsPage.addToCart()
    landingPage.verifyCartQuantity("1")
    print("RESULTS PAGE ENDED")

    cartPage = CartPage(page)
    cartPage.verifyItem(item)
    cartPage.changeQuantity(newQuantity)
    print("CART PAGE")
