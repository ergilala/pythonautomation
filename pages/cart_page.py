from playwright.sync_api import Page, expect


class CartPage:

    def __init__(self, page: Page):
        self.page = page
        self.page.goto("https://www.amazon.com/cart")
        self.quantity = page.locator("xpath=//select[@id='quantity']")
        self.product_name = page.locator("xpath=//div[@class='sc-list-item-content']//span[contains(.,'Mouse')]")
        self.quantity_value = page.locator("xpath=//span[@data-a-class=\"quantity\"]")

    def verifyItem(self, text: str):
        expect(self.product_name.first).to_contain_text(text, ignore_case=True)

    def changeQuantity(self,quantity: str):
        self.page.wait_for_load_state('load')
        self.quantity.select_option(quantity)

    def getQuantity(self) -> int:
        quantity = self.quantity_value.inner_text().split(':')[1]
        return int(quantity)
