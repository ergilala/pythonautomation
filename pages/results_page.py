from playwright.sync_api import Page


class ResultsPage:

    def __init__(self, page: Page):
        self.page = page
        self.add_to_cart_button = page.locator("xpath=//div[@cel_widget_id=\"MAIN-SEARCH_RESULTS-2\"]//button["
                                               "contains(.,'cart')]")
        self.product_name = page.locator("xpath=//div[@data-cy='title-recipe']")

    def addToCart(self):
        self.add_to_cart_button.first.wait_for()
        self.page.wait_for_load_state('load')
        self.page.evaluate(
            '''document.querySelector("button.a-button-text").click()''')  #running a js code for clicking
        # self.add_to_cart_button.first.click(force=True)
