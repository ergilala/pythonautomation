from playwright.sync_api import Page, expect


class LandingPage:

    def __init__(self, page: Page):
        self.page = page
        self.url = 'https://www.amazon.com/'
        self.search_input = page.get_by_placeholder("Search Amazon")
        self.search_button = page.locator("xpath=//input[@id='nav-search-submit-button']")
        self.cart_quantity = page.locator("xpath=//span[@id='nav-cart-count']")

    def navigateToLandingPage(self):
        self.page.goto(self.url)

    def searchForItem(self, text):
        self.search_input.click()
        self.search_input.fill(text)
        self.search_button.click()

    def verifyCartQuantity(self, quantity):
        expect(self.cart_quantity).to_contain_text(quantity)
