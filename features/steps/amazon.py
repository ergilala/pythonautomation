from behave import given, when, then

from pages.landing_page import LandingPage
from pages.cart_page import CartPage
from pages.results_page import ResultsPage


@given("the user is on landing page")
def navigateOnLandingPage(context):
    context.landing_page = LandingPage(context.page)
    context.landing_page.navigateToLandingPage()


@when("the user search for \"{text}\" in search")
def searchForItem(context, text: str):
    context.landing_page.searchForItem(text)


@when("the user adds to cart")
def addToCart(context):
    context.results_page = ResultsPage(context.page)
    context.results_page.addToCart()
    context.landing_page.verifyCartQuantity("1")


@when("the user goes to cart")
def goesToCart(context):
    context.cart_page = CartPage(context.page)


@then("the user edits the quantity to \"{quantity}\"")
def editQuantity(context,quantity: str):
    context.cart_page.changeQuantity(quantity)


@then("the user verifes the quantity changed to \"{quantity}\"")
def verifyQuantity(context, quantity: str):
    actualQuantity = context.cart_page.getQuantity()
    assert int(quantity) == context.cart_page.getQuantity(), f"Expected {quantity}, but got {actualQuantity}"
