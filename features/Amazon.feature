Feature: Amazon Test Cases

  Scenario: Edit quantity of the product
    Given the user is on landing page
    When the user search for "mouse" in search
    When the user adds to cart
    When the user goes to cart
    Then the user edits the quantity to "3"
    Then the user verifes the quantity changed to "3"