from playwright.sync_api import sync_playwright


def before_all(context):
    context.playwright = sync_playwright().start()
    context.browser = context.playwright.chromium.launch(headless=True)
    print("Browser started")


def after_all(context):
    context.browser.close()
    context.playwright.stop()
    print("Browser closed")


def before_scenario(context, scenario):
    context.page = context.browser.new_page()
    context.landing_page = None
    context.cart_page = None
    context.results_page = None
    print("New page  initialized for scenario:", scenario.name)
